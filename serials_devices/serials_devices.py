#!/usr/bin/env python3

import yaml
import sys

def serials_devices():
  supported_options = get_supported_options()
  filename, output_option = get_user_options()

  # User chosen an unsupported option, so stop here.
  if output_option not in supported_options:
    print(f'{output_option} is not supported.')
    options_string = ', '.join(supported_options)
    print(f'The supported options are {options_string}.')
    return

  # Load map from yaml file.
  with open(filename) as data:
    serial_map = yaml.safe_load(data)

  # Loop through the serial map.
  for category, computers in serial_map.items():
    # Sort the list of computers.
    sorted_computers = sorted(computers.items(), key=lambda x: x[1])

    # Print all computers in format for Jamf script.
    if output_option == supported_options['jamf']:
      for serial, name in sorted_computers:
        print(f'elif [ "$ID" == \'{serial}\' ]; then')
        print(f"  /usr/local/bin/jamf setComputerName -name '{name}'")
    # Print list of computer names by category.
    else:
      if (category == output_option) or ('all' == output_option):
        print(f'{category.title()} computers:')
        print(list_computers(sorted_computers))

def get_user_options():
  # Get name of yaml file to read.
  filename = sys.argv[1]

  # Get the output type filter. If no argument is passed, default to jamf.
  supported_options = get_supported_options()
  if 2 == len(sys.argv):
    output_option = supported_options['jamf']
  else:
    output_option = sys.argv[2]

  return filename, output_option

def get_supported_options():
  supported_options = {
    'jamf': 'jamf',
    'all': 'all',
    'dev': 'dev',
    'testing': 'testing',
    'desktop': 'desktop',
    'laptop': 'laptop',
  }
  return supported_options

# Format and list the required computers.
def list_computers(computers):
  # Create a list of computer names prefixed with '*' and ignore the serial
  # numbers.
  list_items = [f'* {name}' for serials, name in computers]
  # Combine all values of the list into a single string with each item on items
  # its own line.
  return ('\n'.join(list_items))

try:
  serials_devices()

# Yaml file must be passed as the first argument after the file name.
except (IndexError):
  supported_options = get_supported_options()
  options_string = '|'.join(supported_options)
  print(f'usage: {sys.argv[0]} serial_file [{options_string}]')

except yaml.YAMLError as exception:
  print(exception)
