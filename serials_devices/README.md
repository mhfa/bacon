# serials_devices

## About
Lists the serial map for MHFA devices for Jamf scripting.
This script is written for Python 3.7 and above.

## Installation & Usage
In the folder root:
```bash
# Create virtual environment. Required only on first run.
python3 -m venv ./venv
# Activate virtual environment. Required each run.
source venv/bin/activate
# Install requirements for the script. Required only on first run.
pip install -r requirements.txt
```
```bash
# Run script.
./serials_devices.py <serial_file> [jamf|all|dev|testing|desktop|laptop]
# Example 1 - using default option.
./serials_devices.py example.yml
# Example 2 - laptop only.
./serials_devices.py example.yml laptop
# Example 3 - all computers in a list instead of the format for Jamf.
./serials_devices.py example.yml all
```
Output options (all output is sorted alphabetically):
* jamf (default) - Displays list formatted for our Jamf script.
* all - Displays a list of all the categories and the associated computer names.
* dev - Displays a list of all the dev computer names.
* testing - Displays a list of all the testing computer names.
* desktop - Displays a list of all the desktop computer names.
* laptop - Displays a list of all the laptop computer names.
```bash
# Exit virtual environment.
deactivate
```
