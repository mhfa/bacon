# hibp

## About
A simple python script utilising the Have I Been Pwned email checking API (https://haveibeenpwned.com).
This script is written for Python 3.6 and above.

## Installation & Usage
In the folder root:
```bash
# Create virtual environment. Required only on first run.
python3 -m venv ./venv
# Activate virtual environment. Required each run.
source venv/bin/activate
# Install requirements for the script. Required only on first run.
pip install -r requirements.txt
# Run script.
python hibp.py
# Exit virtual environment.
deactivate
```
