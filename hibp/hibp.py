import requests

PWNED_API_URL = 'https://haveibeenpwned.com/api/v2/breachedaccount/'
HEADERS = {'User-Agent': 'Test'}

email = ''
while 'exit' != email:
    email = input('Please enter the email to be checked (type \'exit\' to quit): ')
    # Exit the program.
    if 'exit' == email:
        print('Bye!')
    # Check for empty input. If empty, ask for input again.
    elif not email.strip():
        continue
    # Input value is not empty. Run check.
    else:
        response = requests.get(f'{PWNED_API_URL}{email}', HEADERS)
        http_status = response.status_code

        # There has been a breach recorded.
        if 200 == http_status:
            results = response.json()
        # No breach has been recorded.
        elif 404 == http_status:
            results = None
            print('No breaches have been found! Hooray!')
        # Another error.
        else:
            results = None
            print('An error occurred.')

        # Print details if there has been a breach recorded.
        if results is not None:
            print('This email was breached from:')
            for result in results:
                print(result['Title'] + ' on ' + result['BreachDate'])
